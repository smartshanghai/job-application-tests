# Job Application Tests by Category

We make products in both English and Chinese, but since our project management artifacts and comments (on Slack and in e-mail etc) often are in English, 

we prefer that our job applicants are at least able to understand written English (using Google or Baidu Translate is ok). 

Please complete all of the tasks below. Pay attention to code cleanliness and structure.

Feel free to attach a document (.MD or .PDF) explaining your thinking and pointing out strengths and/or weaknesses in your solutions.

## Front-end Development

### 1. Recreate a Design from an Image

Recreate [this](https://cdn.dribbble.com/users/1424150/screenshots/3345884/profile-card_03.07.17.png) design using HTML and CSS.
Pay attention to details, code cleanliness, naming, and styling conventions. Whatever graphics you use to substitute icons etc is not important,
as long as the overall look and feel of the result is similar.

### 2. Implement a Carousel

On our sites we rely on Javascript to animate things and add front-end interactivity.

1. Implement a JavaScript carousel library of your choice, that shows several items from the previous test. The library may require jQuery or a similar dependency.

2. Add buttons that will allow the user to move the carousel back and forth.

## Database Development

Model the database tables and relationships in a system that would be used in a book library for lending out books to customers (No coding necessary).

You may use pen and paper, or any modeling software of your choice. Submit the result as a PDF attachment. Feel free to write comments that explains the reasoning behind your decisions.

## PHP Development

Write a page that has a form. The form should contain one text box that accepts a URL.

Write some PHP that uses a 3rd party library to generate and output a QR code that links to the URL entered in the form.

The code should run in your local environment, and not make any calls to external services.